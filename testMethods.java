
class testMethods {

    public static void main (String[] args) {
        //testSubstr();
        testBracketBuddy();
    }

    public static  void testSubstr() {
        System.out.println(Substring.subseq("texti", "textexti"));
    }

    private static void testBracketBuddy() {
        System.out.println(BracketBuddy.bracketCheck("()[{()}[]](<><>)"));
    }
}
