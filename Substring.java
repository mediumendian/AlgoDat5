
class Substring {

    public static boolean subseq(String t, String p) {
        if (t.length() < p.length())
            return false;

        if (p.length() == 0) { return true; }

        int j = 0;

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == p.charAt(j))
                j++;
            else
                j = 0;

            if (j == p.length()) return true;
        }
        return false;
    }
} // evil code, doesn't cover selfcontaining patterns.
