import java.util.Stack;

class BracketBuddy {

    public static boolean bracketCheck(String brackets) {
        if(!isBracket(brackets))
            return false;

        Stack<Character> bStack = new Stack<>();

        char current;
        for (int i = 0; i < brackets.length(); i++) {
            current = brackets.charAt(i);

            if(isOpening(current)) bStack.push(current);                // push opening bracket on stack
            else if (bStack.isEmpty()) return false;                    // pushing closing bracket on empty stack: wrong
            else if (matches(bStack.peek(), current)) bStack.pop();     // if top and current match, pop().
            else return false;                                          // neither of the above: closing bracket pushed on nonempty stack without match
        }
        return bStack.isEmpty();                                        // if not empty, some brackets weren't closed
    }

    private static boolean isBracket (String brackets) { return brackets.matches("[\\(\\)\\[\\]\\{\\}\\<\\>]*");
    }

    private static boolean isOpening(char brack) { return brack == '(' || brack == '[' || brack == '{' || brack == '<';
    }

    private static boolean matches(char opening, char closing) {
        switch (opening) {
            case '(':
                return closing == ')';
            case '[':
                return closing == ']';
            case '{':
                return closing == '}';
            case '<':
            	return closing == '>';
        }
        throw new IllegalArgumentException("Invalid brackets: " + opening + ", " + closing);
    }
}


